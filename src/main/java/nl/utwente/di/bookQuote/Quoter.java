package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    private HashMap<String, Double> isbnPrice;
    public Quoter() {
        isbnPrice = new HashMap<>();
        isbnPrice.put("1", 10.0);
        isbnPrice.put("2", 45.0);
        isbnPrice.put("3", 20.0);
        isbnPrice.put("4", 35.0);
        isbnPrice.put("5", 50.0);
    }
    public double getBookPrice(String isbn) {
        if(isbnPrice.containsKey(isbn)) {
            return isbnPrice.get(isbn);
        }
        else return 0;
    }
}
